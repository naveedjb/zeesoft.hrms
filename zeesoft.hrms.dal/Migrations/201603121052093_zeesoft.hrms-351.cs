namespace zeesoft.hrms.dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zeesofthrms351 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ShiftMasters", "ShiftCode", "dbo.TblEmployee");
            DropForeignKey("dbo.ShiftDetails", "ShiftCode", "dbo.TblEmployee");
            DropForeignKey("dbo.ActivityCalendars", "Employee_ShiftCode", "dbo.TblEmployee");
            DropIndex("dbo.ShiftDetails", new[] { "ShiftCode" });
            DropIndex("dbo.ShiftMasters", new[] { "ShiftCode" });
            RenameColumn(table: "dbo.ActivityCalendars", name: "Employee_ShiftCode", newName: "Employee_ID");
            RenameIndex(table: "dbo.ActivityCalendars", name: "IX_Employee_ShiftCode", newName: "IX_Employee_ID");
            DropPrimaryKey("dbo.TblEmployee");
            DropPrimaryKey("dbo.ShiftMasters");
            AddColumn("dbo.ShiftDetails", "Employee_ID", c => c.Int());
            AlterColumn("dbo.TblEmployee", "ShiftCode", c => c.String(maxLength: 128));
            AlterColumn("dbo.ShiftMasters", "ShiftCode", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.TblEmployee", "ID");
            AddPrimaryKey("dbo.ShiftMasters", "ShiftCode");
            CreateIndex("dbo.TblEmployee", "ShiftCode");
            CreateIndex("dbo.ShiftDetails", "Employee_ID");
            AddForeignKey("dbo.TblEmployee", "ShiftCode", "dbo.ShiftMasters", "ShiftCode");
            AddForeignKey("dbo.ShiftDetails", "Employee_ID", "dbo.TblEmployee", "ID");
            AddForeignKey("dbo.ActivityCalendars", "Employee_ID", "dbo.TblEmployee", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActivityCalendars", "Employee_ID", "dbo.TblEmployee");
            DropForeignKey("dbo.ShiftDetails", "Employee_ID", "dbo.TblEmployee");
            DropForeignKey("dbo.TblEmployee", "ShiftCode", "dbo.ShiftMasters");
            DropIndex("dbo.ShiftDetails", new[] { "Employee_ID" });
            DropIndex("dbo.TblEmployee", new[] { "ShiftCode" });
            DropPrimaryKey("dbo.ShiftMasters");
            DropPrimaryKey("dbo.TblEmployee");
            AlterColumn("dbo.ShiftMasters", "ShiftCode", c => c.Int(nullable: false));
            AlterColumn("dbo.TblEmployee", "ShiftCode", c => c.Int(nullable: false));
            DropColumn("dbo.ShiftDetails", "Employee_ID");
            AddPrimaryKey("dbo.ShiftMasters", "ID");
            AddPrimaryKey("dbo.TblEmployee", "ShiftCode");
            RenameIndex(table: "dbo.ActivityCalendars", name: "IX_Employee_ID", newName: "IX_Employee_ShiftCode");
            RenameColumn(table: "dbo.ActivityCalendars", name: "Employee_ID", newName: "Employee_ShiftCode");
            CreateIndex("dbo.ShiftMasters", "ShiftCode");
            CreateIndex("dbo.ShiftDetails", "ShiftCode");
            AddForeignKey("dbo.ActivityCalendars", "Employee_ShiftCode", "dbo.TblEmployee", "ShiftCode");
            AddForeignKey("dbo.ShiftDetails", "ShiftCode", "dbo.TblEmployee", "ShiftCode", cascadeDelete: true);
            AddForeignKey("dbo.ShiftMasters", "ShiftCode", "dbo.TblEmployee", "ShiftCode", cascadeDelete: true);
        }
    }
}
