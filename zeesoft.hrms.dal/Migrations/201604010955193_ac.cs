namespace zeesoft.hrms.dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ac : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityCalendars",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TransYear = c.String(),
                        TransDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        TransBehaviour = c.String(),
                        GZH_Status = c.Boolean(nullable: false),
                        GZH_Eligibility = c.String(),
                        Remarks = c.String(),
                        IsGZH = c.Boolean(nullable: false),
                        ShiftCode = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TblEmployee",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SystemCode = c.String(),
                        EmployeeCode = c.String(),
                        Title = c.String(),
                        FirstName = c.String(),
                        ShiftCode = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ShiftMasters", t => t.ShiftCode)
                .Index(t => t.ShiftCode);
            
            CreateTable(
                "dbo.ShiftMasters",
                c => new
                    {
                        ShiftCode = c.String(nullable: false, maxLength: 128),
                        ID = c.Int(nullable: false, identity: true),
                        SystemCode = c.String(),
                        Title = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.ShiftCode);
            
            CreateTable(
                "dbo.ShiftDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SystemCode = c.String(),
                        WEFDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        Shift_WO1 = c.String(),
                        Shift_WO2 = c.String(),
                        Shift_HD = c.String(),
                        MarginTime = c.String(),
                        ShiftStart = c.String(),
                        ShiftEnd = c.String(),
                        ShiftRestStart = c.String(),
                        ShiftRestEnd = c.String(),
                        WorkingHour = c.String(),
                        EligibleHour = c.String(),
                        SNo = c.String(),
                        TotalWorkedHrs = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalEligibleHrs = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ShiftCode = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ShiftMasters", t => t.ShiftCode)
                .Index(t => t.ShiftCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShiftDetails", "ShiftCode", "dbo.ShiftMasters");
            DropForeignKey("dbo.TblEmployee", "ShiftCode", "dbo.ShiftMasters");
            DropIndex("dbo.ShiftDetails", new[] { "ShiftCode" });
            DropIndex("dbo.TblEmployee", new[] { "ShiftCode" });
            DropTable("dbo.ShiftDetails");
            DropTable("dbo.ShiftMasters");
            DropTable("dbo.TblEmployee");
            DropTable("dbo.ActivityCalendars");
        }
    }
}
