namespace zeesoft.hrms.dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zeesofthrms : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActivityCalendars",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ShiftCode = c.String(),
                        Employee_ShiftCode = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TblEmployee", t => t.Employee_ShiftCode)
                .Index(t => t.Employee_ShiftCode);
            
            CreateTable(
                "dbo.TblEmployee",
                c => new
                    {
                        ShiftCode = c.Int(nullable: false),
                        ID = c.Int(nullable: false, identity: true),
                        SysEmployeeCode = c.Int(nullable: false),
                        SystemCode = c.String(),
                        EmployeeCode = c.String(),
                        ApplicationNo = c.String(),
                        Status = c.String(),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        MiddleName = c.String(),
                        FullName = c.String(),
                        PS_FirstName = c.String(),
                        PS_LastName = c.String(),
                        PS_MiddleName = c.String(),
                        PS_FullName = c.String(),
                        DepartmentCode = c.String(),
                        DesignationCode = c.String(),
                        GradeCode = c.String(),
                        JoiningDate = c.DateTime(nullable: false),
                        ConfirmationDate = c.DateTime(nullable: false),
                        DateLeft = c.DateTime(nullable: false),
                        LeftReason = c.String(),
                        LeftType = c.String(),
                        CNIC = c.String(),
                        Region = c.String(),
                        Gender = c.String(),
                    })
                .PrimaryKey(t => t.ShiftCode);
            
            CreateTable(
                "dbo.ShiftDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SystemCode = c.String(),
                        WEFDate = c.DateTime(nullable: false),
                        Shift_WO1 = c.String(),
                        Shift_WO2 = c.String(),
                        Shift_HD = c.String(),
                        MarginTime = c.String(),
                        ShiftStart = c.String(),
                        ShiftEnd = c.String(),
                        ShiftRestStart = c.String(),
                        ShiftRestEnd = c.String(),
                        WorkingHour = c.String(),
                        EligibleHour = c.String(),
                        SNo = c.String(),
                        TotalWorkedHrs = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalEligibleHrs = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ShiftCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TblEmployee", t => t.ShiftCode, cascadeDelete: true)
                .Index(t => t.ShiftCode);
            
            CreateTable(
                "dbo.ShiftMasters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SystemCode = c.String(),
                        Code = c.String(),
                        Title = c.String(),
                        Status = c.String(),
                        ShiftCode = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TblEmployee", t => t.ShiftCode, cascadeDelete: true)
                .Index(t => t.ShiftCode);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ShiftMasters", "ShiftCode", "dbo.TblEmployee");
            DropForeignKey("dbo.ShiftDetails", "ShiftCode", "dbo.TblEmployee");
            DropForeignKey("dbo.ActivityCalendars", "Employee_ShiftCode", "dbo.TblEmployee");
            DropIndex("dbo.ShiftMasters", new[] { "ShiftCode" });
            DropIndex("dbo.ShiftDetails", new[] { "ShiftCode" });
            DropIndex("dbo.ActivityCalendars", new[] { "Employee_ShiftCode" });
            DropTable("dbo.ShiftMasters");
            DropTable("dbo.ShiftDetails");
            DropTable("dbo.TblEmployee");
            DropTable("dbo.ActivityCalendars");
        }
    }
}
