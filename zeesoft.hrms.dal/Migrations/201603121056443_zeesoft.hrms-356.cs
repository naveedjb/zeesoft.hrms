namespace zeesoft.hrms.dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zeesofthrms356 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.TblEmployee", "SysEmployeeCode");
            DropColumn("dbo.TblEmployee", "ApplicationNo");
            DropColumn("dbo.TblEmployee", "Status");
            DropColumn("dbo.TblEmployee", "Title");
            DropColumn("dbo.TblEmployee", "FirstName");
            DropColumn("dbo.TblEmployee", "LastName");
            DropColumn("dbo.TblEmployee", "MiddleName");
            DropColumn("dbo.TblEmployee", "FullName");
            DropColumn("dbo.TblEmployee", "PS_FirstName");
            DropColumn("dbo.TblEmployee", "PS_LastName");
            DropColumn("dbo.TblEmployee", "PS_MiddleName");
            DropColumn("dbo.TblEmployee", "PS_FullName");
            DropColumn("dbo.TblEmployee", "DepartmentCode");
            DropColumn("dbo.TblEmployee", "DesignationCode");
            DropColumn("dbo.TblEmployee", "GradeCode");
            DropColumn("dbo.TblEmployee", "JoiningDate");
            DropColumn("dbo.TblEmployee", "ConfirmationDate");
            DropColumn("dbo.TblEmployee", "DateLeft");
            DropColumn("dbo.TblEmployee", "LeftReason");
            DropColumn("dbo.TblEmployee", "LeftType");
            DropColumn("dbo.TblEmployee", "CNIC");
            DropColumn("dbo.TblEmployee", "Region");
            DropColumn("dbo.TblEmployee", "Gender");
            DropColumn("dbo.ShiftMasters", "Code");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ShiftMasters", "Code", c => c.String());
            AddColumn("dbo.TblEmployee", "Gender", c => c.String());
            AddColumn("dbo.TblEmployee", "Region", c => c.String());
            AddColumn("dbo.TblEmployee", "CNIC", c => c.String());
            AddColumn("dbo.TblEmployee", "LeftType", c => c.String());
            AddColumn("dbo.TblEmployee", "LeftReason", c => c.String());
            AddColumn("dbo.TblEmployee", "DateLeft", c => c.DateTime(nullable: false));
            AddColumn("dbo.TblEmployee", "ConfirmationDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.TblEmployee", "JoiningDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.TblEmployee", "GradeCode", c => c.String());
            AddColumn("dbo.TblEmployee", "DesignationCode", c => c.String());
            AddColumn("dbo.TblEmployee", "DepartmentCode", c => c.String());
            AddColumn("dbo.TblEmployee", "PS_FullName", c => c.String());
            AddColumn("dbo.TblEmployee", "PS_MiddleName", c => c.String());
            AddColumn("dbo.TblEmployee", "PS_LastName", c => c.String());
            AddColumn("dbo.TblEmployee", "PS_FirstName", c => c.String());
            AddColumn("dbo.TblEmployee", "FullName", c => c.String());
            AddColumn("dbo.TblEmployee", "MiddleName", c => c.String());
            AddColumn("dbo.TblEmployee", "LastName", c => c.String());
            AddColumn("dbo.TblEmployee", "FirstName", c => c.String());
            AddColumn("dbo.TblEmployee", "Title", c => c.String());
            AddColumn("dbo.TblEmployee", "Status", c => c.String());
            AddColumn("dbo.TblEmployee", "ApplicationNo", c => c.String());
            AddColumn("dbo.TblEmployee", "SysEmployeeCode", c => c.Int(nullable: false));
        }
    }
}
