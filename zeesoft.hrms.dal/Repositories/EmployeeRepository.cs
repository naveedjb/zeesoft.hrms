﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.dal
{
    public class EmployeeRepository : IEmployeeRepository
    {
         private readonly HrmsContext db;
    
    public EmployeeRepository(HrmsContext context)
    {
        db = context;
    }

    public IEnumerable<Employee> SelectAll()
    {
        return db.Employees.ToList();
    }

    public Employee SelectByID(string id)
    {
        return db.Employees.Find(id);
    }

    public void Insert(Employee obj)
    {
        db.Employees.Add(obj);
    }

    public void Update(Employee obj)
    {
        db.Entry(obj).State = EntityState.Modified;
    }

    public void Delete(string id)
    {
        Employee existing = db.Employees.Find(id);
        db.Employees.Remove(existing);
    }

    public void Save()
    {
        db.SaveChanges();
    }

    }
}