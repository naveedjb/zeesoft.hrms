﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.dal
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> SelectAll();
        Employee SelectByID(string id);
        void Insert(Employee obj);
        void Update(Employee obj);
        void Delete(string id);
        void Save();
    }
}
