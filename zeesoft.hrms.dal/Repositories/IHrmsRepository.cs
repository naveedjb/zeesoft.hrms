﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.dal
{
    interface IHrmsRepository
    {
        Employee Get(int id);
        void Insert(Employee book);
    }
}
