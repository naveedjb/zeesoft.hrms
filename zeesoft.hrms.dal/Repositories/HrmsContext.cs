﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.dal
{
    public class HrmsContext : DbContext, IDisposable
    {//"Data Source=(local);Initial Catalog=zeesoft.hrms;Integrated Security=True;" 

               public HrmsContext()
            : base("Server=.;database=zeesoft.hrms;Integrated Security=True")
        { 
        
        }
        public HrmsContext(DbConnection connection)
            : base("Server=.;database=zeesoft.hrms;Integrated Security=True")
        {

        }
 
        public DbSet<Employee> Employees { get; set; }
        public DbSet<ShiftDetail> ShiftDetails { get; set; }
        public DbSet<ShiftMaster> ShiftMaster { get; set; }
        public DbSet<ActivityCalendar> ActivityCalendar { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) { 
            modelBuilder.Entity<Employee>().ToTable("TblEmployee"); 
            base.OnModelCreating(modelBuilder); }
    
    
    }


}