﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zeesoft.hrms.Models
{
    public class ShiftMaster
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string SystemCode { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        [Key]
        public string ShiftCode { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }

    }
}