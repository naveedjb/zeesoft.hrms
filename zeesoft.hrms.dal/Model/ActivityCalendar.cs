﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zeesoft.hrms.Models
{
    public class ActivityCalendar
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]       
        public int ID { get; set; }

        public string TransYear { get; set; }
         [Column(TypeName = "datetime2")]
        public DateTime? TransDate { get; set; }
        public string TransBehaviour { get; set; }
        public bool GZH_Status { get; set; }
        public string GZH_Eligibility { get; set; }
        public string Remarks { get; set; }
        public bool IsGZH { get; set; }
        public string ShiftCode { get; set; }
    }
}