﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zeesoft.hrms.dal;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.bll
{
    public class BLL_EmployeeMaster
    {
        private IGenericRepository<Employee> _repository = new GenericRepository<Employee>();

        public int ID { get; set; }
        // public int SysEmployeeCode { get; set; }        
        public string SystemCode { get; set; }
        public string EmployeeCode { get; set; }
        //public string ApplicationNo { get; set; }
        //public string Status { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string MiddleName { get; set; }
        //public string FullName { get; set; }
        //public string PS_FirstName { get; set; }
        //public string PS_LastName { get; set; }
        //public string PS_MiddleName { get; set; }
        //public string PS_FullName { get; set; }
        //public string DepartmentCode { get; set; }
        //public string DesignationCode { get; set; }
        //public string GradeCode { get; set; }
        //[ForeignKey(""]
        public string ShiftCode { get; set; }
        //public DateTime JoiningDate { get; set; }
        //public DateTime ConfirmationDate { get; set; }
        //public DateTime DateLeft { get; set; }       
        //public string LeftReason { get; set; }
        //public string LeftType { get; set; }
        //public string CNIC { get; set; }
        //public string Region { get; set; }
        //public string Gender { get; set; }

        public BLL_EmployeeMaster()
        {
            Mapper.CreateMap<BLL_EmployeeMaster, Employee>();
        }

        public void AddOrUpdate(BLL_EmployeeMaster emp)
        {
            var employee = Mapper.Map<Employee>(emp);
            var item = _repository.SelectByID(employee.ID);
           if (item == null)
           {
               _repository.Insert(item);
           }
           else
           {
               _repository.Update(item);
           }
        }
        public List<BLL_EmployeeMaster> GetAll()
        {
            var empList= _repository.SelectAll().ToList<Employee>();
            List<BLL_EmployeeMaster> empViews =   Mapper.Map<List<Employee>, List<BLL_EmployeeMaster>>(empList);
            return empViews;
        }

        public BLL_EmployeeMaster GetByID(int id)
        {
            var emp=_repository.SelectByID(id);
            var employee = Mapper.Map<BLL_EmployeeMaster>(emp);
            return employee;
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }


    }
}
