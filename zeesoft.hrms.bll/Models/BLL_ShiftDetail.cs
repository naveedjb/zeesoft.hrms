﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zeesoft.hrms.dal;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.bll
{
    public class BLL_ShiftDetail
    {
        
        private IGenericRepository<ShiftDetail> _repository = new GenericRepository<ShiftDetail>();

        public int ID { get; set; }
        public string SystemCode { get; set; }
     
        public DateTime? WEFDate { get; set; }
        public string Shift_WO1 { get; set; }
        public string Shift_WO2 { get; set; }
        public string Shift_HD { get; set; }
        public string MarginTime { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public string ShiftRestStart { get; set; }
        public string ShiftRestEnd { get; set; }
        public string WorkingHour { get; set; }
        public string EligibleHour { get; set; }
        public string SNo { get; set; }
        public decimal TotalWorkedHrs { get; set; }
        public decimal TotalEligibleHrs { get; set; }

        public string ShiftCode { get; set; }
        public virtual ShiftMaster ShiftMaster { get; set; }
    

        public BLL_ShiftDetail()
        {
            Mapper.CreateMap<BLL_ShiftDetail, ShiftDetail>();
        }

        public void AddOrUpdate(BLL_ShiftDetail emp)
        {
            var employee = Mapper.Map<ShiftDetail>(emp);
            var item = _repository.SelectByID(employee.ID);
           if (item == null)
           {
               _repository.Insert(item);
           }
           else
           {
               _repository.Update(item);
           }
        }
        public List<BLL_ShiftDetail> GetAll()
        {
            var empList= _repository.SelectAll().ToList<ShiftDetail>();
            List<BLL_ShiftDetail> empViews = Mapper.Map<List<ShiftDetail>, List<BLL_ShiftDetail>>(empList);
            return empViews;
        }

        public BLL_ShiftDetail GetByID(int id)
        {
            var emp=_repository.SelectByID(id);
            var employee = Mapper.Map<BLL_ShiftDetail>(emp);
            return employee;
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

    }
}
