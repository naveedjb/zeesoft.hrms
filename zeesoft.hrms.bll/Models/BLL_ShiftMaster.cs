﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zeesoft.hrms.dal;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.bll
{
    public class BLL_ShiftMaster
    {
        private IGenericRepository<ShiftMaster> _repository = new GenericRepository<ShiftMaster>();
        public int ID { get; set; }
        public string SystemCode { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }

        public BLL_ShiftMaster()
        {
            Mapper.CreateMap<BLL_ShiftMaster, ShiftMaster>();
        }

        public void AddOrUpdate(BLL_ShiftMaster shift)
        {
            var sft = Mapper.Map<ShiftMaster>(shift);
            var item = _repository.SelectByID(sft.ShiftCode);
           if (item == null)
           {
               _repository.Insert(item);
           }
           else
           {
               _repository.Update(item);
           }
        }
        public List<BLL_ShiftMaster> GetAll()
        {
            var empList = _repository.SelectAll().ToList<ShiftMaster>();
            List<BLL_ShiftMaster> empViews = Mapper.Map<List<ShiftMaster>, List<BLL_ShiftMaster>>(empList);
            return empViews;
        }

        public BLL_ShiftMaster GetByID(int id)
        {
            var emp=_repository.SelectByID(id);
            var employee = Mapper.Map<BLL_ShiftMaster>(emp);
            return employee;
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
