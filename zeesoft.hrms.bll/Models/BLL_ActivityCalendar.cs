﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zeesoft.hrms.dal;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.bll
{
    class BLL_ActivityCalendar
    {
          private IGenericRepository<ActivityCalendar> _repository = new GenericRepository<ActivityCalendar>();

        public int ID { get; set; }

        public string TransYear { get; set; }       
        public DateTime? TransDate { get; set; }
        public string TransBehaviour { get; set; }
        public bool GZH_Status { get; set; }
        public string GZH_Eligibility { get; set; }
        public string Remarks { get; set; }
        public bool IsGZH { get; set; }
        public string ShiftCode { get; set; }


        public BLL_ActivityCalendar()
        {
            Mapper.CreateMap<BLL_ActivityCalendar, ActivityCalendar>();
        }

        public void AddOrUpdate(BLL_ActivityCalendar emp)
        {
            var employee = Mapper.Map<ActivityCalendar>(emp);
            var item = _repository.SelectByID(employee.ID);
           if (item == null)
           {
               _repository.Insert(item);
           }
           else
           {
               _repository.Update(item);
           }
        }
        public List<BLL_ActivityCalendar> GetAll()
        {
            var empList= _repository.SelectAll().ToList<ActivityCalendar>();
            List<BLL_ActivityCalendar> empViews = Mapper.Map<List<ActivityCalendar>, List<BLL_ActivityCalendar>>(empList);
            return empViews;
        }

        public BLL_ActivityCalendar GetByID(int id)
        {
            var emp=_repository.SelectByID(id);
            var employee = Mapper.Map<BLL_ActivityCalendar>(emp);
            return employee;
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
