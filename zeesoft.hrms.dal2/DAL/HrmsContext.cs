﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.DAL
{
    public class HrmsContext : DbContext
    {

        public HrmsContext() { }
        public HrmsContext(DbConnection connection) : base(connection, true)
{
 
}
 
        public DbSet<Employee> Employees { get; set; }
        public DbSet<ShiftDetail> ShiftDetails { get; set; }
        public DbSet<ShiftMaster> ShiftMaster { get; set; }
        public DbSet<ActivityCalendar> ActivityCalendar { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) { 
            modelBuilder.Entity<Employee>().ToTable("TblEmployee"); 
            base.OnModelCreating(modelBuilder); }
    
    
    }


}