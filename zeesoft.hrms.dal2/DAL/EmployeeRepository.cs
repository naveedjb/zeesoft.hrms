﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using zeesoft.hrms.DAL;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.DAL
{
    public class EmployeeRepository : IEmployeeRepository
    {
         private readonly HrmsContext _context;
    
    public EmployeeRepository(HrmsContext context)
    {
        _context = context;
    }
    
    public Employee Get(int id)
    {
        return _context.Employees.SingleOrDefault(x => x.ID == id);
    }
    
    public void Insert(Employee employee)
    {
        _context.Employees.Add(employee);
        _context.SaveChanges();
    }

    }
}