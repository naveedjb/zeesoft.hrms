﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zeesoft.hrms.Models
{
    public class Employee
    {

        public Employee()
        {
            
        }
         public Employee(string FName) 
    {
        FirstName = FName;
    }
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int SysEmployeeCode { get; set; }        
        public string SystemCode { get; set; }        
        public string EmployeeCode { get; set; }
        public string ApplicationNo { get; set; }
        public string Status { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string FullName { get; set; }
        public string PS_FirstName { get; set; }
        public string PS_LastName { get; set; }
        public string PS_MiddleName { get; set; }
        public string PS_FullName { get; set; }
        public string DepartmentCode { get; set; }
        public string DesignationCode { get; set; }
        public string GradeCode { get; set; }
        [Key]
        public int ShiftCode { get; set; }
        public DateTime JoiningDate { get; set; }
        public DateTime ConfirmationDate { get; set; }
        public DateTime DateLeft { get; set; }       
        public string LeftReason { get; set; }
        public string LeftType { get; set; }
        public string CNIC { get; set; }
        public string Region { get; set; }
        public string Gender { get; set; }
        //public virtual ICollection<ShiftMaster> Enrollments { get; set; }
    }
}