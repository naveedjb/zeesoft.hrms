﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zeesoft.hrms.Models
{
    public class ShiftMaster
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string SystemCode { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }

        public int ShiftCode { get; set; }
        public virtual Employee Employee { get; set; }

    }
}