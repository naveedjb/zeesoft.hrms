﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace zeesoft.hrms.Models
{
    public class ShiftDetail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string SystemCode { get; set; }
       
        public DateTime WEFDate { get; set; }
        public string Shift_WO1 { get; set; }
        public string Shift_WO2 { get; set; }
        public string Shift_HD { get; set; }
        public string MarginTime { get; set; }
        public string ShiftStart { get; set; }
        public string ShiftEnd { get; set; }
        public string ShiftRestStart { get; set; }
        public string ShiftRestEnd { get; set; }
        public string WorkingHour { get; set; }
        public string EligibleHour { get; set; }
        public string SNo { get; set; }
        public decimal TotalWorkedHrs { get; set; }
        public decimal TotalEligibleHrs { get; set; }

        public int ShiftCode { get; set; }
        public virtual Employee Employee { get; set; }
    }
}