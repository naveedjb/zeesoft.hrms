﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using zeesoft.hrms.dal;
using Effort;
using zeesoft.hrms.Models;
using FluentAssertions;


namespace zeesoft.hrms.dal.tests
{
    [TestClass]
    public class UnitTest1
    {
        private HrmsContext _context;
        private IGenericRepository<Employee> _EmployeeRepository = new GenericRepository<Employee>();
        //    _ShiftRepository = new GenericRepository<ShiftMaster>();;
        private IGenericRepository<ShiftMaster> _ShiftRepository= new GenericRepository<ShiftMaster>();
        private IGenericRepository<ShiftDetail> _ShiftDetailRepository = new GenericRepository<ShiftDetail>();

        private IGenericRepository<ActivityCalendar> _ActivityCalendarRepository = new GenericRepository<ActivityCalendar>();

        //[TestInitialize]
        //public void Initialize()
        //{
        //    var connection = DbConnectionFactory.CreateTransient();
        //    _context = new HrmsContext(connection);
        //    _repository = new GenericRepository<Employee>();
        //    _ShiftRepository = new GenericRepository<ShiftMaster>();
        //}



        [TestMethod]
        public void InsertShiftMaster()
        {
          
            var sm = new ShiftMaster { ShiftCode = "c" };
            _ShiftRepository.Insert(sm);
            _ShiftRepository.Save();

        }

        [TestMethod]
        public void ReadShiftMaster()
        {
           
            var retrievedShift = _ShiftRepository.SelectAll();
            // Assert
            retrievedShift.Should().HaveCount(1);            
        }

        [TestMethod]
        public void UpdateShiftMaster()
        {

            var retrievedShift = _ShiftRepository.SelectByID("c");
            retrievedShift.Title = "Jang";
            _ShiftRepository.Update(retrievedShift);
            _ShiftRepository.Save();   
          
        }

     

        [TestMethod]
        public void InsertEmployee()
        {
            // Arrange
            const string name = "Zee";
            var emp = new Employee { FirstName = name, ShiftCode = "c" };
            _EmployeeRepository.Insert(emp);
            _EmployeeRepository.Save();
        }

       

        [TestMethod]
        public void ReadEmployee()
        {

            var retrievedEmployees = _EmployeeRepository.SelectAll();
            // Assert
            retrievedEmployees.Should().HaveCount(1);
           
        }

        [TestMethod]
        public void UpdateEmployee()
        {

            var retrievedEmployee = _EmployeeRepository.SelectByID(4);
            retrievedEmployee.FirstName= "Jang";
            _EmployeeRepository.Update(retrievedEmployee);
            _EmployeeRepository.Save();

        }

        [TestMethod]
        public void DeleteEmployee()
        {

            var retrievedEmployees = _EmployeeRepository.SelectAll();
            var retEmp = _EmployeeRepository.First<Employee>(retrievedEmployees);
            _EmployeeRepository.Delete(retEmp.ID);
            _EmployeeRepository.Save();
           
        }

        [TestMethod]
        public void InsertShiftDetail()
        {
            var emp = new ShiftDetail { WEFDate = new DateTime(), ShiftCode = "c" };
            _ShiftDetailRepository.Insert(emp);
            _ShiftDetailRepository.Save();
        }



        [TestMethod]
        public void ReadShiftDetail()
        {

            var retrievedDetail = _ShiftDetailRepository.SelectAll();
            // Assert
            retrievedDetail.Should().HaveCount(1);            
        }


        [TestMethod]
        public void DeleteShiftDetail()
        {
            _ShiftDetailRepository.Delete(1);
            _ShiftDetailRepository.Save();
            
        }

        [TestMethod]
        public void InsertActivityCalendar()
        {   
            var emp = new ActivityCalendar { TransDate = new DateTime(), ShiftCode = "c" };
            _ActivityCalendarRepository.Insert(emp);
            _ActivityCalendarRepository.Save();
        }



        [TestMethod]
        public void ReadActivityCalendar()
        {

            var retrievedCalendar = _ActivityCalendarRepository.SelectAll();
            // Assert
            retrievedCalendar.Should().HaveCount(1);            
        }


        [TestMethod]
        public void DeleteActivityCalendar()
        {
            var retrievedCalendar = _ActivityCalendarRepository.SelectAll();
            var retEmp = _ActivityCalendarRepository.First<ActivityCalendar>(retrievedCalendar);
            _ActivityCalendarRepository.Delete(retEmp.ID);           
            _ActivityCalendarRepository.Save();
            
        }

        [TestMethod]
        public void DeleteShiftMaster()
        {

            var retrievedShift = _ShiftRepository.SelectByID("c");
            _ShiftRepository.Delete(retrievedShift.ShiftCode);
            _ShiftRepository.Save();

        }
    }
}
