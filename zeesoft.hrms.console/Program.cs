﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zeesoft.hrms.dal;
using zeesoft.hrms.Models;

namespace zeesoft.hrms.console
{
    class Program
    {
        static void Main(string[] args)
        {
            //using (var db = new HrmsContext())
            //{
            //    db.ShiftMaster.Add(new ShiftMaster { ShiftCode=Guid.NewGuid().ToString(), Title = "Another Blog " });
            //    db.SaveChanges();

            //    foreach (var blog in db.Blogs)
            //    {
            //        Console.WriteLine(blog.Name);
            //    }
            //}
         //   Database.SetInitializer(new MigrateDatabaseToLatestVersion<HrmsContext, System.Net.Configuration>()); 
            GenericRepository<ShiftMaster> sm = new GenericRepository<ShiftMaster>();
            sm.Insert(new ShiftMaster { ShiftCode = Guid.NewGuid().ToString(), Title = "Another Blog " });
            sm.Save();
            foreach (var blog in sm.SelectAll())
            {
                Console.WriteLine(blog.Title);
            }
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(); 
        }
    }
}
